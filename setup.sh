#! /bin/bash

# Bootstrapping script to set up project, can remove once it's done

function usage() {
    cat << EOF
Setup script for django-template project.

Usage:

${0} <project_name>

EOF
}

if [[ -z ${1} ]]; then
    echo "Please provide a project name."
    exit 1
fi

find . -type f -not -path '*/.git/*' -exec sed -i "s/django_template/${1}/g" {} \;
mv django_template ${1}

rm -i setup.sh
