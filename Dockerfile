FROM python:3.7.0-alpine

RUN addgroup -S uwsgi && adduser -S uwsgi -G uwsgi

RUN mkdir -p /app && chown uwsgi /app

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev linux-headers bash

COPY --chown=uwsgi:uwsgi requirements.txt .
COPY --chown=uwsgi:uwsgi build/requirements-docker.txt .

RUN pip install --no-cache-dir -r requirements.txt -r requirements-docker.txt

USER uwsgi
WORKDIR /app

COPY --chown=uwsgi:uwsgi . .

CMD [ "/app/build/run.sh" ]
