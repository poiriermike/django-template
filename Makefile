#PIP_INSTALLED := $(which pip3 > /dev/null)

#ifndev PIP_INSTALLED
#	echo "pip3 not detected. Enesure python3 and pip3 are installed on the system"
#endif

all: build db test

clean:
	docker image rm --force django_template:local

db:
	docker-compose up -d database

build: Dockerfile
	docker image build -t django_template:local .

flake:
	flake8

test: db flake
	DATABASE_NAME=django_template \
	DATABASE_USERNAME=mike \
	DATABASE_PASSWORD=pass \
	SECRET_KEY=")h$$1y!$#&py2(=um=3=+9go-+z)9kf*sc04a@eh775ifxy27$$=n" \
	coverage run manage.py test
	coverage report
setup:
	pip3 install pew
	pew new -p python3 django_template
	pew in django_template pip install -r requirements.txt
run: db
	python manage.py runserver

compose:
	docker-compose up -d
