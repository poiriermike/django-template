#! /bin/bash

# Functions

function check_variables_set () {
    VARIABLE=$1
    echo $VARIABLE
    echo ${!VARIABLE}
    if [[ -z ${!VARIABLE} ]]; then
        echo "$VARIABLE not set"
        exit 1
    fi
}

# Variables

DATABASE_PORT=${DATABASE_PORT:-5432}


check_variables_set DATABASE_USERNAME
check_variables_set DATABASE_PASSWORD
check_variables_set DATABASE_HOST
check_variables_set DATABASE_PORT
check_variables_set DATABASE_NAME
check_variables_set SECRET_KEY

uwsgi --socket 0.0.0.0:3031 \
               --uid uwsgi \
               --plugins python3, \
               --protocol uwsgi \
               --wsgi django_template.wsgi:application
