# Django Template

Template repository for Django projects.

## Setup

Run the `setup.sh` script with the name of your project.

## Development

Start a local development server by running `make run`.

Build a docker image with `make build`

Run tests with `make test` or just `make`

Clean up docker artifacts with `make clean`

Happy Djangoing.

## License

This project is unlicensed. Do with it what you will. It is provided without warranty.
